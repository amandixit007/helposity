(function ()
{
    'use strict';

    angular
        .module('app.signup',[]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider,  $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.signup', {
            url    : '/signup',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/signup/signup.html',
                    controller : 'SignupController as vm'
                }
            },
                // resolve: {
                //     SampleData: function (msApi)
                //     {
                //         return msApi.resolve('sample@get');
                //     }
                // }
        });

         // Translation
        $translatePartialLoaderProvider.addPart('admin/main');

        // Api
        // msApiProvider.register('signup', ['app/data/sample/sample.json']);

        // Navigation
        // msNavigationServiceProvider.saveItem('fuse', {
        //     title : 'SIGNUP',
        //     group : true,
        //     weight: 1
        // });

        // msNavigationServiceProvider.saveItem('fuse.signup', {
        //     title    : 'Signup',
        //     icon     : 'icon-tile-four',
        //     state    : 'app.signup',
        //     /*stateParams: {
        //         'param1': 'page'
        //      },*/
        //     translate: 'SIGNUP',
        //     weight   : 1
        // });

    }

})();