(function ()
{
    'use strict';

    angular
        .module('app.school',[]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider,  $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.school', {
            url    : '/school',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/school/school.html',
                    controller : 'schoolController as vm'
                }
            },
                // resolve: {
                //     SampleData: function (msApi)
                //     {
                //         return msApi.resolve('sample@get');
                //     }
                // }
        });

         // Translation
        $translatePartialLoaderProvider.addPart('admin/main');

        // Api
        // msApiProvider.register('signup', ['app/data/sample/sample.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse', {
            title : 'SCHOOL',
            group : true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('fuse.user', {
            title    : 'School Management',
            icon     : 'icon-tile-four',
            state    : 'app.school',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'School Management',
            weight   : 3
        });

    }

})();