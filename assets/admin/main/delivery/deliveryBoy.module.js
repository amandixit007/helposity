(function ()
{
    'use strict';

    angular
        .module('app.deliveryBoy',
            [
                // 3rd Party Dependencies
                // 'xeditable'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.deliveryBoy', {
            url    : '/deliveryBoy',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/delivery/deliveryBoy.html',
                    controller : 'deliveryBoyController as vm'
                }
            }
            // resolve: {
            //     Contacts: function (msApi)
            //     {
            //         return msApi.resolve('deliveryBoy.contacts@get');
            //     },
            //     User: function (msApi)
            //     {
            //         return msApi.resolve('deliveryBoy.user@get');
            //     }
            // }
        });

        // Translation
        $translatePartialLoaderProvider.addPart('admin/main/deliveryBoy');

        // Api
        msApiProvider.register('contacts.contacts', ['admin/data/contacts/contacts.json']);
        msApiProvider.register('contacts.user', ['admin/data/contacts/user.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('deliveryBoy', {
            title : 'Delivery Boy',
            icon  : 'icon-account-box',
            state : 'app.deliveryBoy',
            weight: 10
        });

    }

})();