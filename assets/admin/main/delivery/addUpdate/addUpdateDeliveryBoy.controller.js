(function ()
{
    'use strict';

    angular
        .module('app.auth.userManagement')
        .controller('addUpdateDeliveryBoyController', addUpdateDeliveryBoyController);

    /** @ngInject */
    function addUpdateDeliveryBoyController($scope,$mdDialog, Contact,userService, Contacts, User, msUtils)
    {
        var vm = this;

        // Data
        vm.title = 'Edit Contact';
        vm.contact = angular.copy(Contact);
        vm.contacts = Contacts;
        vm.user = User;
        vm.newContact = false;
        vm.allFields = false;

        if ( !vm.contact )
        {
            vm.contact = {
                'id'      : msUtils.guidGenerator(),
                'name'    : '',
                'lastName': '',
                'avatar'  : 'assets/images/avatars/profile.jpg',
                'nickname': '',
                'company' : '',
                'jobTitle': '',
                'email'   : '',
                'phone'   : '',
                'address' : '',
                'birthday': null,
                'notes'   : ''
            };

            vm.title = 'New Contact';
            vm.newContact = true;
            vm.contact.tags = [];
        }

        // Methods
        vm.addNewContact = addNewContact;
        vm.saveContact = saveContact;
        vm.deleteContactConfirm = deleteContactConfirm;
        vm.closeDialog = closeDialog;
        vm.toggleInArray = msUtils.toggleInArray;
        // vm.exists = msUtils.exists;

        //////////

        /**
         * Add new contact
         */

         $scope.upload = function(){
            console.log($('#file').prop('files'))
         }

        function addNewContact()
        {
            // vm.contacts.unshift(vm.contact);
            
            vm.delivery.type="deliverBoy";
            console.log("vm.delivery =>",vm.delivery)
            
             userService.deliveryBoyAdd(vm.delivery)
            .then(objS=>{
                console.log("Result ===>"+JSON.stringify(objS))
                if(objS.responseCode == 200){
                   closeDialog();
                }
                else
                    $mdDialog.show(
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Joka Lake')
                        .textContent(objS.response_message)
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Close')
                        
                    );
            },objE=>{
                console.log(objE)
            })
            
        }

        /**
         * Save contact
         */
        function saveContact()
        {
            // Dummy save action
            for ( var i = 0; i < vm.contacts.length; i++ )
            {
                if ( vm.contacts[i].id === vm.contact.id )
                {
                    vm.contacts[i] = angular.copy(vm.contact);
                    break;
                }
            }

            closeDialog();
        }

        /**
         * Delete Contact Confirm Dialog
         */
        function deleteContactConfirm(ev)
        {
            var confirm = $mdDialog.confirm()
                .title('Are you sure want to delete the contact?')
                .htmlContent('<b>' + vm.contact.name + ' ' + vm.contact.lastName + '</b>' + ' will be deleted.')
                .ariaLabel('delete contact')
                .targetEvent(ev)
                .ok('OK')
                .cancel('CANCEL');

            $mdDialog.show(confirm).then(function ()
            {

                vm.contacts.splice(vm.contacts.indexOf(Contact), 1);

            });
        }

        /**
         * Close dialog
         */
        function closeDialog()
        {
            $mdDialog.hide();
        }

    }
})();