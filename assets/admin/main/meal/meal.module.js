(function ()
{
    'use strict';

    angular
        .module('app.meal',[]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider,  $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.meal', {
            url    : '/meal',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/meal/meal.html',
                    controller : 'mealController as vm'
                }
            },
                // resolve: {
                //     SampleData: function (msApi)
                //     {
                //         return msApi.resolve('sample@get');
                //     }
                // }
        });

         // Translation
        $translatePartialLoaderProvider.addPart('admin/main');

        // Api
        // msApiProvider.register('signup', ['app/data/sample/sample.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('fuse', {
            title : 'MEAL',
            group : true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('fuse.meal', {
            title    : 'Meal Management',
            icon     : 'icon-tile-four',
            state    : 'app.meal',
            /*stateParams: {
                'param1': 'page'
             },*/
            translate: 'Meal Management',
            weight   : 10
        });

    }

})();