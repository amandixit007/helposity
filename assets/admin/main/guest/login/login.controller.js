(function ()
{
    'use strict';

    angular
        .module('app.guest.login')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController( $scope,$state,LoginService,$mdDialog)
    {
        var vm = this;
        vm.login={}
        vm.login.type= 'admin';
        // Methods
        vm.loginFn = function(){
            LoginService.login(vm.login)
            .then(objS=>{"local",
               console.log(objS)
                if(objS.responseCode == 200){
                    var dataLogin = {};
                        dataLogin.userName = objS.result.userName;
                        dataLogin.email_id = objS.result.email_id;
                        dataLogin.phone_no = objS.result.phone_no;
                        dataLogin.type = objS.result.type;
                        localStorage.token = objS.token;

                    localStorage.adminLogin = JSON.stringify(dataLogin); 
                     $state.go('app.dashboard');   
                }
                else
                    $mdDialog.show(
                      $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Joka Lake')
                        .textContent(objS.reponseMessage)
                        .ariaLabel('Alert Dialog Demo')
                        .ok('Close')
                        
                    );
            },objE=>{
                console.log(objE)
            })
        }
    }
})();