(function ()
{
    'use strict';

    angular
        .module('app.guest.login')
        .service('LoginService', LoginService);

    /** @ngInject */
    function LoginService($http, $q)
    {
      const url = 'http://v57.sdnsbox.com:1101/api';
        // Data
        let self = this

        // Methods
        self.signup = function(data){
            const defer = $q.defer();

            $http({
                method: 'POST',
                url: url+'/signup',
                data: data
            }).
            then(objS=>{
                defer.resolve(objS);
            },objE=>{
                defer.reject(objE);
            })
            return defer.promise;
        },

        self.login = function(data){
            const defer = $q.defer();
            $http({
                method: 'POST',
                url: url+'/login',
                data: data
            }).
            then(objS=>{
                defer.resolve(objS.data);
            },objE=>{
                defer.reject(objE.data);
            })
            return defer.promise;
        }
    }
})();