(function ()
{
    'use strict';

    angular
        .module('app.guest.login', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.login', {
            url      : '/login',
            views    : {
                'main@'                       : {
                    templateUrl: 'admin/core/layouts/content-only.html',
                    controller : 'MainController as vm'
                },
                'content@app.login': {
                    templateUrl: 'admin/main/guest/login/login.html',
                    controller : 'LoginController as vm'
                }
            },
            bodyClass: 'login'
        });

        // Translation
        $translatePartialLoaderProvider.addPart('admin/main/guest/login');

        // Navigation
        // msNavigationServiceProvider.saveItem('pages.auth', {
        //     title : 'Authentication',
        //     icon  : 'icon-lock',
        //     weight: 1
        // });

        // msNavigationServiceProvider.saveItem('login', {
        //     title : 'Login',
        //     state : 'app.login',
        //     weight: 1
        // });
    }

})();