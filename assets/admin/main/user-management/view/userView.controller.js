(function ()
{
    'use strict';

    angular
        .module('app.auth.userView')
        .controller('userViewController', userViewController);

    /** @ngInject */
    function userViewController($state,$scope, userService,$mdSidenav, msUtils, $mdDialog, $document)
    {

        var vm = this;
        console.log("id ====>",$state.params.id)
        // Data
            userService.userView($state.params.id)
            .then(objS=>{
               console.log("data =>"+JSON.stringify(objS))
                if(objS.responseCode == 200){
                    vm.userDetail = objS.result;
                }
            },objE=>{
                console.log(objE)
            })

    }
})();