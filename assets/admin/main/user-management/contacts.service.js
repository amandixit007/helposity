(function ()
{
    'use strict';

    angular
        .module('app.auth.userManagement')
        .service('userService', userService);

    /** @ngInject */
    function userService($http, $q)
    {
      const url = 'http://v57.sdnsbox.com:1101/api';
        // Data
        let self = this

        // Methods
        self.userList = function(type){
            const defer = $q.defer();

            $http({
                method: 'GET',
                url: url+'/user_list/'+type,
            }).
            then(objS=>{
                defer.resolve(objS.data);
            },objE=>{
                defer.reject(objE.data);
            })
            return defer.promise;
        }

        self.userDelete = function(data){
            const defer = $q.defer();

            $http({
                method: 'POST',
                url: url+'/user_delete',
                data:data
            }).
            then(objS=>{
                defer.resolve(objS.data);
            },objE=>{
                defer.reject(objE.data);
            })
            return defer.promise;
        }

        self.userView = function(id){
            const defer = $q.defer();

            $http({
                method: 'GET',
                url: url+'/user_view/'+id,
            }).
            then(objS=>{
                defer.resolve(objS.data);
            },objE=>{
                defer.reject(objE.data);
            })
            return defer.promise;
        }

        self.deliveryBoyAdd = function(data){
            const defer = $q.defer();

            $http({
                method: 'POST',
                url: url+'/addDeliveryBoy',
                data:data
            }).
            then(objS=>{
                defer.resolve(objS.data);
            },objE=>{
                defer.reject(objE.data);
            })
            return defer.promise;
        }
    }
})();