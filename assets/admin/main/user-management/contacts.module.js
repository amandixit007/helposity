(function ()
{
    'use strict';

    angular
        .module('app.auth.userManagement',
            [
                // 3rd Party Dependencies
                // 'xeditable'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        $stateProvider.state('app.contacts', {
            url    : '/contacts',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/user-management/contacts.html',
                    controller : 'ContactsController as vm'
                }
            },
            resolve: {
                Contacts: function (msApi)
                {
                    return msApi.resolve('contacts.contacts@get');
                },
                User: function (msApi)
                {
                    return msApi.resolve('contacts.user@get');
                }
            }
        });

        // Translation
        $translatePartialLoaderProvider.addPart('admin/main/user-management');

        // Api
        msApiProvider.register('contacts.contacts', ['admin/data/contacts/contacts.json']);
        msApiProvider.register('contacts.user', ['admin/data/contacts/user.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('contacts', {
            title : 'User Management',
            icon  : 'icon-account-box',
            state : 'app.contacts',
            weight: 1
        });

    }

})();