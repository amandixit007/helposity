(function ()
{
    'use strict';

    angular
        .module('app.auth.dashboard',
            [
                // 3rd Party Dependencies
                // 'nvd3'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider.state('app.dashboard', {
            url      : '/dashboard',
            views    : {
                'content@app': {
                    templateUrl: 'admin/main/auth/dashboard/dashboard.html',
                    controller : 'DashboardController as vm'
                }
            },
            resolve  : {
                DashboardData: function (msApi)
                {
                    return msApi.resolve('dashboard.analytics@get');
                }
            },
            bodyClass: 'dashboard'
        });

        // Api
        msApiProvider.register('dashboard.analytics', ['admin/data/dashboard/analytics/data.json']);

        msNavigationServiceProvider.saveItem('navigation', {
            title : 'NAVIGATION',
            group : true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('navigation.dashboard', {
            title: 'Dashboard',
            state: 'app.dashboard'
        });
    }

})();