(function ()
{
    'use strict';

    angular
        .module('app.support', [])
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        // State
        $stateProvider.state('app.support', {
            url    : '/support',
            views  : {
                'content@app': {
                    templateUrl: 'admin/main/auth/support/support.html',
                    controller : 'supportController as vm'
                }
            },
            resolve: {
                Contacts: function (msApi)
                {
                    return msApi.resolve('chat.chats@get');
                },
                User    : function (msApi)
                {   
                    return msApi.resolve('chat.user@get');
                }
            }
        });

        // Translation
        $translatePartialLoaderProvider.addPart('admin/main/auth/support');

        // Api
        // Contacts data must be alphabatically ordered.
        msApiProvider.register('chat.contacts', ['admin/data/support/contacts.json']);
        msApiProvider.register('chat.chats', ['admin/data/support/supports/:id.json']);
        msApiProvider.register('chat.user', ['admin/data/support/user.json']);

        // Navigation
        msNavigationServiceProvider.saveItem('support', {
            title : 'support',
            icon  : 'icon-hangouts',
            state : 'app.support',
            /*badge : {
                content: 13,
                color  : '#09d261'
            },*/
            weight: 5
        });
    }

})();