(function ()
{
    'use strict';

    angular
        .module('app.quick-panel', [])
        .config(config);

    /** @ngInject */
    function config($translatePartialLoaderProvider, msApiProvider)
    {
        // Translation
        $translatePartialLoaderProvider.addPart('admin/quick-panel');

        // Api
        msApiProvider.register('quickPanel.activities', ['admin/data/quick-panel/activities.json']);
        msApiProvider.register('quickPanel.contacts', ['admin/data/quick-panel/contacts.json']);
        msApiProvider.register('quickPanel.events', ['admin/data/quick-panel/events.json']);
        msApiProvider.register('quickPanel.notes', ['admin/data/quick-panel/notes.json']);
    }
})();
