(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick Panel
            'app.quick-panel',

            // Guest Pages
            'app.guest.login',

            // Auth Pages
            'app.auth.dashboard',
            'app.support',
            // 'app.deliveryBoy',

            // Sample
            // 'app.sample',
            // signup
            'app.signup',
            // user
            'app.school',
            'app.meal',
            'app.auth.userManagement',
            'app.auth.userView',
            'app.deliveryBoy'
            // 'app.contacts'
        ])
        
})();